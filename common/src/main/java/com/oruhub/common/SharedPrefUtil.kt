package com.oruhub.common

import android.content.Context
import android.content.SharedPreferences


const val GENERAL_PREF = "mark_general_prefs"
const val PREFS_PRIVATE_MODE = 0

fun getStringFromPreferences(sharedPreferences: SharedPreferences, key: String, defaultValue: String): String?{
    return sharedPreferences.getString(key,defaultValue)
}

fun getStringFromPrefs(context: Context, key: String, defaultValue: String ): String? {
    val sharedPreferences = context.getSharedPreferences(GENERAL_PREF, PREFS_PRIVATE_MODE)
    return sharedPreferences.getString(key, defaultValue)
}

fun getStringFromPrefs(context: Context, key: String) : String?{
    val sharedPreferences = context.getSharedPreferences(GENERAL_PREF, PREFS_PRIVATE_MODE)
    return sharedPreferences.getString(key,"")
}

fun setPrefsString(context: Context, key: String, value: String) {
    context.getSharedPreferences(GENERAL_PREF, PREFS_PRIVATE_MODE).edit()
            .putString(key, value).apply()
}

fun getIntFromPrefs(context: Context, key: String, defaultValue:Int = 0): Int {
    val sharedPreferences = context.getSharedPreferences(GENERAL_PREF, PREFS_PRIVATE_MODE)
    return sharedPreferences.getInt(key, defaultValue)
}

fun setPrefsInt(context: Context, key: String, value: Int) {
    context.getSharedPreferences(GENERAL_PREF, PREFS_PRIVATE_MODE).edit()
            .putInt(key, value).apply()
}


fun getBooleanFromPrefs(context: Context, key: String): Boolean {
    val sharedPreferences = context.getSharedPreferences(GENERAL_PREF, PREFS_PRIVATE_MODE)
    return sharedPreferences.getBoolean(key, false)
}

fun setBooleanFromPrefs(context: Context, key: String, boolean: Boolean){
    context.getSharedPreferences(GENERAL_PREF, PREFS_PRIVATE_MODE).edit()
            .putBoolean(key,boolean).apply()
}

fun setIntPrefs(context: Context, key: String, value: Int) {
    context.getSharedPreferences(GENERAL_PREF, PREFS_PRIVATE_MODE).edit()
            .putInt(key, value).apply()
}

fun getIntFromPrefs(context: Context, key: String): Int {
    return context.getSharedPreferences(GENERAL_PREF, PREFS_PRIVATE_MODE).getInt(key, 0)

}