package com.oruhub.categories.network

import com.oruhub.booking.network.CategoriesService
import com.oruhub.networks.retrofitClient


object CategoriesApi {
    val categoriesService: CategoriesService by lazy {
        retrofitClient().create(CategoriesService::class.java)
    }
}