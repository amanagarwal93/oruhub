package com.oruhub.android

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class AddMoreServicesFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_more_services, container, false)
    }

    companion object {
        fun newInstance(param1: String?, param2: String?): AddMoreServicesFragment {
            val fragment = AddMoreServicesFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}