package com.oruhub.profile.network

import com.oruhub.booking.network.ProfileService
import com.oruhub.networks.retrofitClient


object ProfileApi {
    val profileService: ProfileService by lazy {
        retrofitClient().create(ProfileService::class.java)
    }
}