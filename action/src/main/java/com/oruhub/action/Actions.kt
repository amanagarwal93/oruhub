package com.oruhub.action

import android.content.Context
import android.content.Intent

object Actions {


// open profile feature

    fun openProfileIntent(context: Context) =
        internalIntent(context, "com.oruhub.profile.open")

    fun openBookingIntent(context: Context) =
            internalIntent(context, "com.oruhub.booking.open")

    fun openCategoryIntent(context: Context) =
            internalIntent(context, "com.oruhub.categories.open")

    fun openHomeIntent(context: Context) =
            internalIntent(context, "com.oruhub.home.open")


    private fun internalIntent(context: Context, action: String) =
        Intent(action).setPackage(context.packageName)

}
