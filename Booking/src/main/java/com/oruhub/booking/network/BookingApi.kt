package com.oruhub.booking.network

import com.oruhub.networks.retrofitClient


object BookingApi {
    val bookingService: BookingService by lazy {
        retrofitClient().create(BookingService::class.java)
    }
}