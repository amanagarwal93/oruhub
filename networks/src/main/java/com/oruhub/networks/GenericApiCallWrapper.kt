package com.oruhub.networks

import com.squareup.moshi.Moshi
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

suspend fun <T> safeApiCall(
        apiCall: suspend () -> Response<T>
): ResultWrapper<Response<T>> {
    return try {
        val response = apiCall.invoke()
        val responseCode = response.code()
        if (response.isSuccessful && (responseCode in 200..299))
            ResultWrapper.Success(response)
        else {
            ResultWrapper.GenericError(responseCode, convertErrorBody(response.errorBody()!!.string()))
        }
    } catch (throwable: Throwable) {
        when (throwable) {
            is IOException -> ResultWrapper.NetworkError
            is HttpException -> {
                val code = throwable.code()
                val errorResponse = convertErrorBody(throwable)
                ResultWrapper.GenericError(code, errorResponse)
            }
            else -> {
                ResultWrapper.GenericError(null, null)
            }
        }
    }
}

private fun convertErrorBody(throwable: HttpException): ErrorResponse? {
    return try {
        throwable.response()?.errorBody()?.source()?.let {
            val moshiAdapter = Moshi.Builder().build().adapter(ErrorResponse::class.java)
            moshiAdapter.fromJson(it)
        }
    } catch (exception: Exception) {
        null
    }
}

private fun convertErrorBody(string: String): ErrorResponse? {
    val moshiAdapter = Moshi.Builder().build().adapter(ErrorResponse::class.java)
    return moshiAdapter.fromJson(string)
}