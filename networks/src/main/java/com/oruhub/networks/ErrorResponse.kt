package com.oruhub.networks

data class ErrorResponse(
        val code : String,
        val reasons : String,
        val errorCode : String
)